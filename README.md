# Terpa
Terpa is a simple password tool. That is meant to replace GUI programs like keepassxc or bitwarden. It is also was made to easily see and use passwords between different OS/devices.

# Set Up Process:

#1: Make sure you have all necassary programs installed.

#2: Change the file variable to a file on your computer.

#3: BOOM! Simple password management!

#4: Make sure you encrypt the file which stores the passwords.

# What needs to be added:
- [ ] Password search
- [ ] Copys password to clipboard after being generated
- [ ] Added options when the programs is launched such as to open the password file
